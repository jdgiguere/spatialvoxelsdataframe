SpatialVoxelsDataFrame
=======================

:Author: Jean-Denis Giguere
:Copying: GPL (>= 2)


About
-----
An implementation of SpatialVoxelsDataFrame for R extending 
the SpatialGridDataFrame to 3D.

