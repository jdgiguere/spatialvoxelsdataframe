setClass("Spatial3DGridDataFrame",
         representation("SpatialGrid", data = "data.frame"),
         validity = function(object) {
           if (sp:::.NumberOfCells(object@grid) != nrow(object@data))
             stop("unequal number of objects in full grid and data slot")
           stopifnot(dimensions(object)==3)
           return(TRUE)
         }
)
setIs("Spatial3DGridDataFrame","Spatial3DGrid")