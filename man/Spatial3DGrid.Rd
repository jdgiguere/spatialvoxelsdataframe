\name{Spatial3DGrid}
\alias{Spatial3DGrid}
\title{ define 3D spatial grid }
\description{
defines spatial 3D grid by offset, cell size et dimensions
}
\usage{
Spatial3DGrid(grid, proj4string = CRS(as.character(NA)))
}
\arguments{
	\item{grid}{ grid topology; object of class \link[sp]{GridTopology-class} }
	\item{proj4string}{ object of class \link[sp]{CRS-class}}
}
\value{Spatial3DGrid returns an object of class \link{Spatial3DGrid-class}}

\author{ Jean-Denis Giguere, \email{jean-dens.giguere@usherbrooke.ca} }

\seealso{
  \code{\link{Spatial3DGridDataFrame-class}}, \code{\link[sp]{SpatialGrid}}
}

\examples{
library(sp)
x = GridTopology(c(0,0,0), c(1,1,1), c(3,3,3))
class(x)
x
summary(x)
coordinates(x)
y = Spatial3DGrid(grid = x)
class(y)
y
}

\keyword{spatial}
